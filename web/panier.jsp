<%-- 
    Document   : panier
    Created on : 2019-09-30, 16:35:21
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="MyPackage.produitPourPanier,MyPackage.panier"%>
<!DOCTYPE html>
<html>
    <head>
        <title>THE AFFABLE BEAN_PANIER</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Bad Script' rel='stylesheet'>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
         <script type="text/javascript" src="script.js"></script>      
<style>
body {
    font-family: 'David Libre';
    background-color: whitesmoke;

}
</style>
    </head>
    <body>
        <div class="container" >
              <div class="row">
                <div class="col-md-6">
                   <a href="/Epicerie_V2/"><h3 style="font-family: 'Bad Script';font-size:40px;">The Affable bean</h3></a>
                </div>
               
                <div class="col-md-4">
                    <div class="row" style="float:right">
                        <input type="button" value="Francais" class="btn"/>
                        <input type="button" value="Anglais" class="btn"/>
                    </div>
                </div>
            </div>
            
            <% panier p=(panier)session.getAttribute("Panier");
            
            if(p.listProd.size()==0)
            {%>
                    <center><h1 style="margin-top:50px">PANIER VIDE !!! </h1></center>

            <%}
            else
            {%>
                
            
            <table class="table table-sm table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Quantite</th>
      <th scope="col">Nom</th>
      <th scope="col">Prix</th>
      <th scope="col">Sous-Total</th>

    </tr>
  </thead>
  <tbody>
      
  <%for(produitPourPanier pr:p.listProd){ %>    
    <tr>
      <th scope="row"><%=pr.id%></th>
      <th> 
      
          <select name="qantite" class="quantite" id="<%=pr.id%>">
                <% for(int i=0;i<100;i++){%>
                <%if(i==pr.getQuantite()){%>
                <option selected=""><%=i%></option>
                <%}else{%>
              <option><%=i%></option>
              <%}%>
              <%}%>
          </select>
      
      </th>
      <td><%=pr.nom%></td>
      <td>$<%=pr.prix%></td>
      <td>$<%=pr.prix*pr.getQuantite()%></td>

    </tr>
    <%}%>
     <tr>
      <td>Total</td>
      <td></td>
      <td></td>
      <td></td>

      <td>$<%=p.PrixTotal%></td>
    </tr>
  </tbody>
</table>
    <div class="row">
        <div class="col-md-6">
            <a href="/Epicerie_V2/vider" class="card-link" >            
                <input type="button" style="float:left" class="btn" value="Vider le panier"/>
            </a>

        </div>
        <div class="col-md-6">
            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_cart"/>
            <input type="hidden" name="upload" value="1">
            <input type="hidden" name="business" value="sedricaffBusiness@gmail.com">
          <% int cnt=1;
              for(produitPourPanier prod:p.listProd){%>
            <input type="hidden" name="item_name_<%=cnt%>" value="<%=prod.nom%>"/>
            <input type="hidden" name="amount_<%=cnt%>" value="<%=prod.prix%>"/>
            <input type="hidden" name="quantity_<%=cnt%>" value="<%=prod.getQuantite()%>"/>
           
            <% cnt++;}%>
            <input type="hidden" name="currency_code" value="CAD">
            <input type="hidden" name="return" value="https://www.google.com/">
            <input type="hidden" name="cancel_return" value="https://www.bing.com/">
           <input type="submit" class="btn btn-success" style="float:right" value="Passer à la caisse"/>

            </form>

        </div>
    </div>
            <%}
            %>
            
        </div>
    </body>
</html>
