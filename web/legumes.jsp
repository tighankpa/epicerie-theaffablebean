 <%-- 
    Document   : menuProduits
    Created on : 2019-09-17, 14:20:05
    Author     : dell
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="MyPackage.produit,MyPackage.categorie, MyPackage.panier"%>
<!DOCTYPE html>
<html>
    <head>
         <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Bad Script' rel='stylesheet'>
      <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
      <script type="text/javascript" src="script.js"></script>      
  

<style>
body {
    font-family: 'David Libre';
    background-color: whitesmoke;
}
.prod{
    width: 200px;
}

</style>
    </head>
    <body>
       <%
        ArrayList <produit> prod=(ArrayList<produit>)session.getAttribute("produits");
        int quantitePanier=0;
panier lePanier=(panier)session.getAttribute("Panier");
 if(lePanier!=null)
        {
        quantitePanier=lePanier.listProd.size();
        }
       %>
        
        <div class="container">
           
            <div class="row">
                <div class="col-md-6">
                    <a href="/Epicerie_V2/"><h3 style="font-family: 'Bad Script';font-size:40px;">The Affable bean</h3></a>
                </div>
                
                <div class="col-md-2">
                             <a href="/Epicerie_V2/panier">
                                 <img src="https://img.icons8.com/ios-filled/30/000000/shopping-cart.png">
                             </a><span class="badge" id="qtePanier"><%=quantitePanier%></span>
                </div>
                
                
                <div class="col-md-4">
                    <div class="row" style="float:right">
                        <input type="button" value="Francais" class="btn"/>
                        <input type="button" value="Anglais" class="btn"/>
                    </div>
                </div>
            </div>
            
               
            <%
            if(prod.size()==0)
            {   %>
            <center><h1 style="margin-top:50px">Aucun légume !!!</h1></center>
           <% }
            else{   %>
            <div class="row"> 
            <div class="col-md-2">
                             
        <%
            int id=0;
            String desc="";
            String nom="";
            String image="";
            double prix=0; 
            int i=0;
        for(produit p:prod)
        {%>
       <%  
           if(i==0){
              id=p.getId();        
       desc=p.getDesc();
       nom=p.getNom();
       image=p.getImage();
       prix=p.getPrix();
       }i++;
       %>
          <div class="row">
                    <input type="button" class="btn btn-info prod" id="<%=p.getId()%>" value="<%=p.getNom()%>"/>
                    <br/>
          </div>
        
        <%}%> 
                        
              
              
            </div>
            <div class="col-md-10">
                
                <div class="row">
                    <div class="col-md-12">
                        <img id="imageSrc" src="<%= image%>" style="width: 100%; height:550px;"/>
                           <input type="hidden" id="idActif" value="<%= id%>">
                    </div>
                </div>
                <div style="margin-left:12px;">
                 <div class="row">
                     <p><b>Nom : </b> <span id="nom"><%= nom%></span> </p>
           
                </div>
                <div class="row">
                    <p><b>Description : </b> 
                      <br/>
                      <span id="description"><%= desc%></span>
                     </p>
                </div>
                <div class="row">
                 
                    <p><b>Prix : </b><span id="prix"><%= prix%> $</span></p>
                </div>
                </div>
               <div class="row">
                    <div class="col-md-6">    
                    <input class="btn btn-info ajout" type="button" value="Ajouter au panier">
                    </div>  
                    <div class="col-md-6">    
                        <a href="/Epicerie_V2/"><input class="btn btn-info" type="button" value="Retour au menu" style="float: right"></a>
                    </div> 
                </div>    
            </div>
</div>
            <%}%>
         
        </div>
    </body>
</html>
