<%-- 
    Document   : index
    Created on : 2019-11-12, 14:58:29
    Author     : dell
--%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="MyPackage.produit,MyPackage.categorie, MyPackage.panier"%>

<!DOCTYPE html>
<html>
    <head>
               <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Bad Script' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=David Libre' rel='stylesheet'>
        <link rel="stylesheet" href="style.css" type="text/css"/>

        
        <style>
body {
    font-family: 'David Libre';
    background-color: whitesmoke;
}
</style>
    </head>
    <body>
             <%
        ArrayList <produit> prod=(ArrayList<produit>)session.getAttribute("produits");
        int quantitePanier=0;
        panier lePanier=(panier)session.getAttribute("Panier");
        if(lePanier!=null)
        {
        quantitePanier=lePanier.listProd.size();
        }
       %>
  <div class="container" >

            <div class="row">
                
                <div class="col-md-6">
                   <a href="/Epicerie_V2/"><h3 style="font-family: 'Bad Script';font-size:40px;">The Affable bean</h3></a>

                </div>
              <div class="col-md-2">
                             <a href="/Epicerie_V2/panier">
                                 <img src="https://img.icons8.com/ios-filled/30/000000/shopping-cart.png">
                             </a><span class="badge" id="qtePanier"><%=quantitePanier%></span>
                </div>
                <div class="col-md-4">
                    <div class="row" style="float:right">
                        <input type="button" value="Francais" class="btn"/>
                        <input type="button" value="Anglais" class="btn"/>
                    </div>
                </div>
            </div>
             <div class="row">
                    <img src="./Images/legumes.jpg" alt="Image" height="auto" width="100%"/>
            </div>
            <br/>
             <div class="row">
                <div class="col-md-6">
                    <p>
                 La page d'accueil est la page d'accueil du site Web ,
                 et le point de départ de l'application . Elle introduit 
                 l'entreprise et les services à l'utilisateur , et permet à
                 l' utilisateur de naviguer vers l'une des quatre catégories 
                 de produits    
                    </p>
                </div>
                <div class="col-md-6">
                  <div class="row">  
                    <div class="card col-md-6" >
                        <div class="card-body">
                            <div class="row">
                                <img src="./Images/fruits.jpg" alt="Image" height="150px"  width="100%"/>
                            </div>
                             <h5 class="card-title">
                                 Cette categorie  est consacree au fruit
                            </h5>                            
                               <center><a href="/Epicerie_V2/fruits" class="card-link" >Voir les fruits</a></center>
 
                        </div>
                    </div>
                       <div class="card col-md-6" >
                        <div class="card-body">
                            <div class="row">
                                 <img src="./Images/viandes.jpg" alt="Image" height="150px" width="100%"/>
                            </div>
                             <h5 class="card-title">Cette categorie est consacree aux viandes</h5>                             
                               <center><a href="/Epicerie_V2/viandes" class="card-link">Voir les viandes</a>
                                  </center>
                        </div>
                    </div> <div class="card col-md-6" >
                        <div class="card-body">
                            <div class="row">
                                 <img src="./Images/legumes.jpg" alt="Image" height="150px" width="100%"/>
                            </div>
                             <h5 class="card-title">Cette categorie  est consacree aux legumes</h5>                                                 
                               <center><a href="/Epicerie_V2/legumes" class="card-link">Voir les legumes</a></center>

                        </div>
                    </div> 
                      <div class="card col-md-6" >
                        <div class="card-body">
                             <div class="row">
                                 <img src="./Images/laitiers.jpg" alt="Image" height="150px" width="100%"/>
                            </div>
                             <h5 class="card-title"> 
                                 Cette categorie  est consacree aux produits  laitiers

                             </h5>                          
                               <center><a href="/Epicerie_V2/laitiers" class="card-link">Voir les produits  laitiers</a></center>

                        </div>
                    </div>
                  </div>
                </div>
                
            </div>
   </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"  crossorigin="anonymous"></script>
    </body>
</html>
