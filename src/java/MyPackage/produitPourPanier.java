package MyPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dell
 */
public class produitPourPanier {
    public int id;
    public String nom;
    public String description;
    public double prix;
    public String image;
    public String type;
    public int quantite;
    public produitPourPanier(produit p)
    {
         setId(p.id);
        setNom(p.nom);
        setDesc(p.description);
        setPrix(p.prix);
        setImage(p.image);
        setType(p.type);
        setQuantite(1);
    }
      public produitPourPanier (int Id, String Nom,String Description,double Prix,String Image,String Type)
    {
        setId(Id);
        setNom(Nom);
        setDesc(Description);
        setPrix(Prix);
        setImage(Image);
        setType(Type);
    }
    public void setId(int Id)
    {
        this.id=Id;
    }
    public void setNom(String Nom)
    {
        this.nom=Nom;
    }
    public void setDesc(String Description)
    {
        this.description=Description;
    }
    public void setPrix(double Prix)
    {
        this.prix=Prix;
    }
    public void setImage(String Image)
    {
        this.image=Image;
    }
      public void setType(String Type)
    {
        this.type=Type;
    }
    public String getNom()
    {
        return this.nom;
    }
     public String getDesc()
    {
        return this.description;
    }
    public double getPrix()
    {
        return this.prix;
    }
     public String getImage()
    {
        return this.image;
    }
      public int getId()
    {
        return this.id;
    }
        public String getType()
    {
        return this.type;
    }
        public void setQuantite(int qtte)
    {
        this.quantite=qtte;
    }
    public int getQuantite()
    {
        return this.quantite;
    }
    
}
