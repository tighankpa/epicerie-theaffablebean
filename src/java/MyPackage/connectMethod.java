package MyPackage;


import MyPackage.produit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;


public class connectMethod {
    private Connection connexion;
    private String url="jdbc:mysql://localhost:3306/epicerie?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
   
    public ArrayList<produit> recupererProduits(String type) {
        ArrayList <produit> prods = new ArrayList<produit>();
        Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        try {
            statement = connexion.createStatement();
            // Exécution de la requête
            resultat = statement.executeQuery("SELECT * FROM produit where type='"+type+"';");

            // Récupération des données
            while (resultat.next()) {
                int Id=resultat.getInt("idProduit");
                String nom = resultat.getString("nom");
                String description=resultat.getString("description");
                String image =resultat.getString("image");
                double prix = resultat.getDouble("prix");
                String ltype=resultat.getString("type");

//                System.out.println("*********"+nom+description+prix+image+ltype);
                produit prodE = new produit(Id,nom,description,prix,image,ltype);
                prods.add(prodE);
            }
        } catch (SQLException e) {
                            System.out.println(e);

        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ex) {
            System.out.println(ex);

            }
        }
        return prods;
    }
    public produit recupererProduitById(String id) {
        produit prodE=new produit();
        Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        try {
            statement = connexion.createStatement();
            // Exécution de la requête
            resultat = statement.executeQuery("SELECT * FROM produit where idProduit='"+id+"';");

            // Récupération des données
            while (resultat.next()) {
                int Id=resultat.getInt("idProduit");
                String nom = resultat.getString("nom");
                String description=resultat.getString("description");
                String image =resultat.getString("image");
                double prix = resultat.getDouble("prix");
                String ltype=resultat.getString("type");

//                System.out.println("*********"+nom+description+prix+image+ltype);
               prodE = new produit(Id,nom,description,prix,image,ltype);
            }
        } catch (SQLException e) {
                            System.out.println(e);

        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ex) {
            System.out.println(ex);

            }
        }
        return prodE;
    }
    private void loadDatabase() {
        // Chargement du driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
                        System.out.println(e);

        }

        try {
            connexion = DriverManager.getConnection(url, "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}