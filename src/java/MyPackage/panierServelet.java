/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author dell
 */
public class panierServelet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet panierServelet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet panierServelet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        if((panier)session.getAttribute("Panier")==null)
        {
            System.out.println("ENTREE DO_GET");
            panier p=new panier("Le panier");
            session.setAttribute("Panier", p);
            System.out.println("SORTIE DO_GET");

        }
          RequestDispatcher dis=request.getRequestDispatcher("/panier.jsp");
          dis.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id=request.getParameter("idActif");
        connectMethod con=new connectMethod();
        produit prod=con.recupererProduitById(id);
        produitPourPanier Leproduit=new produitPourPanier(prod);

        System.out.println("++LE NOM DU PRODUIT RECUPERE"+Leproduit.nom);
        PrintWriter out=response.getWriter();
        HttpSession session=request.getSession();
        panier Lepanier=new panier("Panier");
        if((panier)session.getAttribute("Panier")==null)
        {
            panier p=new panier("Le panier");
            p.listProd.add(Leproduit);
            p.PrixTotal+=Leproduit.prix;
            session.setAttribute("Panier", p);
            System.out.println("++NOUVELLE SESSION DO_POST "+id);
        }
        else
        {
         Lepanier=(panier)session.getAttribute("Panier");
        int i=0;
        //Quand on fait la boucle for, si un attribut p est modifié, 
        //cela affacte l'objet même, juste pour dire que quand on fait un changement , cela affecte l'objet lui meme
        for(produitPourPanier p:Lepanier.listProd)
        {
            if(p.id==Leproduit.id)
            {
                p.quantite+=1;
                i=1;
                Lepanier.PrixTotal+=Leproduit.prix*1;
            }
        }
        
        if(i!=1)
        {
        Lepanier.listProd.add(Leproduit);
        Lepanier.PrixTotal+=Leproduit.prix*Leproduit.quantite;
        }
        
        session.setAttribute("Panier", Lepanier);
        System.out.println("++ANCIENNE SESSION DO_POST "+id);
        }
        out.print("Produit ajoute au panier !!!"+"*"+Lepanier.listProd.size());
        out.flush();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
