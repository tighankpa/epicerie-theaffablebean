-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 19 déc. 2019 à 22:37
-- Version du serveur :  10.1.31-MariaDB
-- Version de PHP :  7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `epicerie`
--

-- --------------------------------------------------------

--
-- Structure de la table `langue`
--

CREATE TABLE `langue` (
  `idlangue` int(11) NOT NULL,
  `accueilTexteIntroductif` text NOT NULL,
  `accueilVoirPlusFruit` text NOT NULL,
  `accueilVoirPlusViande` text NOT NULL,
  `accueilVoirPlusLegume` text NOT NULL,
  `accueilVoirPlusLaitier` text NOT NULL,
  `accueilTexteFruit` text NOT NULL,
  `accueilTexteViande` text NOT NULL,
  `accueilTexteLegume` text NOT NULL,
  `accueilTexteLait` text NOT NULL,
  `nom` text NOT NULL,
  `description` text NOT NULL,
  `prix` text NOT NULL,
  `ajouterAuPanier` text NOT NULL,
  `retourAuMenu` text NOT NULL,
  `quantite` text NOT NULL,
  `total` text NOT NULL,
  `passerCaisse` text NOT NULL,
  `numero` text NOT NULL,
  `nomLangue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `idProduit` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `prix` double NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`idProduit`, `nom`, `description`, `image`, `prix`, `type`) VALUES
(1, 'Lactantia', 'Desc', './Images/lactantia.jpg', 45, 'laitiers'),
(2, 'Papaye', 'La papaye , celle là vient de Hiti et a été bien traité', './Images/papaye.jpg', 40, 'fruits'),
(3, 'Fraise', 'Fraise du Saskachwan. \r\nCe fruit rouge est botaniquement parlant un faux-fruit ; il s\'agit en réalité d\'un réceptacle charnu sur lequel sont disposés régulièrement des akènes dans des alvéoles plus ou moins profondes. La fraise est donc un polyakène. Quelques fruits d\'autres espèces sans rapport avec Fragaria, et par analogie de forme, portent le nom vernaculaire de « fraise ».\r\n\r\nLa fraise fait partie des 6% environ de végétaux qui existent avec une forme mâle et une forme femelle (et il en existe aussi des formes à la fois mâle et femelle), ce qui a été découvert par un agriculteur non éduqué de l\'Ohio dans les années 1840. Elle se présente sous trois formes : mâle, femelle et combinée1.', './Images/fraise.jpg', 20, 'fruits'),
(4, 'Boeuf hache', 'Boeuf hache bon pour barbecue.\r\nLa viande bovine est la viande issue des animaux de l\'espèce Bos taurus, qu\'il s\'agisse de vache, taureau, veau, broutard, taurillon, génisse ou bœuf. C\'est un produit agricole destiné quasi exclusivement à l\'alimentation humaine.', './Images/boeufhache.jpg', 50, 'Viandes'),
(5, 'Poulet entier', 'Un poulet est une jeune volaille, mâle ou femelle, de la sous-espèce Gallus gallus domesticus, élevée pour sa chair. S\'il s\'agit du même animal, les conditions de production des poulets de chair diffèrent de celles des poules pondeuses qui sont élevées pour leurs œufs.', './Images/poulet.jpg', 700, 'Viandes'),
(6, 'Brocoli', 'Le brocoli est une variété de chou originaire du sud de l\'Italie. Il fut sélectionné par les Romains à partir du chou sauvage. Ceux-ci l\'appréciaient beaucoup et la cuisine italienne l\'utilise beaucoup. Il fut introduit en France par Catherine de Médicis.', './Images/brocoli.jpg', 78, 'Legumes'),
(7, 'Carottes', 'La carotte, Daucus carota subsp. sativus, est une plante bisannuelle de la famille des apiacées, largement cultivée pour sa racine pivotante charnue, comestible, de couleur généralement orangée, consommée comme légume. La carotte représente après la pomme de terre le principal légume-racine cultivé dans le monde.', './Images/carottes.jpg', 10, 'Legumes');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `langue`
--
ALTER TABLE `langue`
  ADD PRIMARY KEY (`idlangue`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`idProduit`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `langue`
--
ALTER TABLE `langue`
  MODIFY `idlangue` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `idProduit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
